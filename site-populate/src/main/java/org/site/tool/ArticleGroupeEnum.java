package org.site.tool;
public enum ArticleGroupeEnum {

	CANAPES_FAUTEUIL("canape"),
	TABLES_RANGEMENT("table"),
	LITTERIE("litterie"),
	CHAMBRES("chambres"),
	BUREAUX("bureau"),
	CUISINE("cuisine"),
	SALLE_DE_BAIN("salle de bain"),
	ELECTROMENAGER_LAVAGE("electromenager"),
	ELECTROMENAGER_FROID("electromenager"),
	ELECTROMENAGER_CUISSON("electromenager"),
	ELECTROMENAGER_SOIN("electromenager"),
	ELECTROMENAGER_CUISINE("electromenager"),


	ORANGE_BANANE_CLEMENTINE("classiques"),
	POMME_RAISIN_POIRE("classiques"),
	PASTHEQUE_MELON("classiques"),
	FURITS_EXOTIQUES("exotiques"),
	FRUITSECS("fruitsecs"),
	AUTRES ("autres")
	;


	String imageDir;
	
	private ArticleGroupeEnum(String imageDir) {
		this.imageDir = imageDir;
	}

	public String getImageDir() {
		return imageDir;
	}

	public static ArticleGroupeEnum parse(String name) {
		for (ArticleGroupeEnum agEnum : ArticleGroupeEnum.values()) {
			if (agEnum.name().equals(name)) {
				return agEnum;
			}
		}
		return null;
	}


}