
package org.site.tool.model.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour menusType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="menusType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="menu" type="{}menuType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="haut" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="droite" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="bas" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="gauche" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="menuBorderTop" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="menuOvale" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="hautOvale" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="droiteOvale" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="basOvale" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="gaucheOvale" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="centreOvale" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="centreTabbed" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="contentPopup" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="showAlertes" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="alertesOvale" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="title" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlRootElement(name="menus")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "menusType", propOrder = {
    "menu"
})
public class MenusType {

    protected List<MenuType> menu;
    @XmlAttribute(name = "haut")
    protected String haut;
    @XmlAttribute(name = "droite")
    protected String droite;
    @XmlAttribute(name = "bas")
    protected String bas;
    @XmlAttribute(name = "gauche")
    protected String gauche;
    @XmlAttribute(name = "menuBorderTop")
    protected String menuBorderTop;
    @XmlAttribute(name = "menuOvale")
    protected String menuOvale;
    @XmlAttribute(name = "hautOvale")
    protected String hautOvale;
    @XmlAttribute(name = "droiteOvale")
    protected String droiteOvale;
    @XmlAttribute(name = "basOvale")
    protected String basOvale;
    @XmlAttribute(name = "gaucheOvale")
    protected String gaucheOvale;
    @XmlAttribute(name = "centreOvale")
    protected String centreOvale;
    @XmlAttribute(name = "centreTabbed")
    protected String centreTabbed;
    @XmlAttribute(name = "contentPopup")
    protected String contentPopup;
    @XmlAttribute(name = "showAlertes")
    protected String showAlertes;
    @XmlAttribute(name = "alertesOvale")
    protected String alertesOvale;
    @XmlAttribute(name = "title")
    protected String title;

    /**
     * Gets the value of the menu property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the menu property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMenu().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MenuType }
     * 
     * 
     */
    public List<MenuType> getMenu() {
        if (menu == null) {
            menu = new ArrayList<MenuType>();
        }
        return this.menu;
    }

    /**
     * Obtient la valeur de la propriété haut.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHaut() {
        return haut;
    }

    /**
     * Définit la valeur de la propriété haut.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHaut(String value) {
        this.haut = value;
    }

    /**
     * Obtient la valeur de la propriété droite.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDroite() {
        return droite;
    }

    /**
     * Définit la valeur de la propriété droite.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDroite(String value) {
        this.droite = value;
    }

    /**
     * Obtient la valeur de la propriété bas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBas() {
        return bas;
    }

    /**
     * Définit la valeur de la propriété bas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBas(String value) {
        this.bas = value;
    }

    /**
     * Obtient la valeur de la propriété gauche.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGauche() {
        return gauche;
    }

    /**
     * Définit la valeur de la propriété gauche.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGauche(String value) {
        this.gauche = value;
    }

    /**
     * Obtient la valeur de la propriété menuBorderTop.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMenuBorderTop() {
        return menuBorderTop;
    }

    /**
     * Définit la valeur de la propriété menuBorderTop.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMenuBorderTop(String value) {
        this.menuBorderTop = value;
    }

    /**
     * Obtient la valeur de la propriété menuOvale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMenuOvale() {
        return menuOvale;
    }

    /**
     * Définit la valeur de la propriété menuOvale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMenuOvale(String value) {
        this.menuOvale = value;
    }

    /**
     * Obtient la valeur de la propriété hautOvale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHautOvale() {
        return hautOvale;
    }

    /**
     * Définit la valeur de la propriété hautOvale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHautOvale(String value) {
        this.hautOvale = value;
    }

    /**
     * Obtient la valeur de la propriété droiteOvale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDroiteOvale() {
        return droiteOvale;
    }

    /**
     * Définit la valeur de la propriété droiteOvale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDroiteOvale(String value) {
        this.droiteOvale = value;
    }

    /**
     * Obtient la valeur de la propriété basOvale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasOvale() {
        return basOvale;
    }

    /**
     * Définit la valeur de la propriété basOvale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasOvale(String value) {
        this.basOvale = value;
    }

    /**
     * Obtient la valeur de la propriété gaucheOvale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGaucheOvale() {
        return gaucheOvale;
    }

    /**
     * Définit la valeur de la propriété gaucheOvale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGaucheOvale(String value) {
        this.gaucheOvale = value;
    }

    /**
     * Obtient la valeur de la propriété centreOvale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCentreOvale() {
        return centreOvale;
    }

    /**
     * Définit la valeur de la propriété centreOvale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCentreOvale(String value) {
        this.centreOvale = value;
    }

    /**
     * Obtient la valeur de la propriété centreTabbed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCentreTabbed() {
        return centreTabbed;
    }

    /**
     * Définit la valeur de la propriété centreTabbed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCentreTabbed(String value) {
        this.centreTabbed = value;
    }

    /**
     * Obtient la valeur de la propriété contentPopup.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentPopup() {
        return contentPopup;
    }

    /**
     * Définit la valeur de la propriété contentPopup.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentPopup(String value) {
        this.contentPopup = value;
    }

    /**
     * Obtient la valeur de la propriété showAlertes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShowAlertes() {
        return showAlertes;
    }

    /**
     * Définit la valeur de la propriété showAlertes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShowAlertes(String value) {
        this.showAlertes = value;
    }

    /**
     * Obtient la valeur de la propriété alertesOvale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlertesOvale() {
        return alertesOvale;
    }

    /**
     * Définit la valeur de la propriété alertesOvale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlertesOvale(String value) {
        this.alertesOvale = value;
    }

    /**
     * Obtient la valeur de la propriété title.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Définit la valeur de la propriété title.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

}
