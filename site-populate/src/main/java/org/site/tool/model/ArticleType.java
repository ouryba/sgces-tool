package org.site.tool.model;

public class ArticleType {

	private long id;
	private String code;
	private String libelle;
	private String description;
	private long groupeId;
	
	private boolean actif;








	public ArticleType(long id, String code, String libelle, String description,
			long groupeId, boolean actif) {
		super();
		this.id = id;
		this.code = code;
		this.libelle = libelle;
		this.description = description;
		this.groupeId = groupeId;
		this.actif = actif;
	}



	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getGroupeId() {
		return groupeId;
	}
	public void setGroupeId(long groupeId) {
		this.groupeId = groupeId;
	}





	public boolean isActif() {
		return actif;
	}





	public void setActif(boolean actif) {
		this.actif = actif;
	}



	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ArticleType [id=");
		builder.append(id);
		builder.append(", code=");
		builder.append(code);
		builder.append(", libelle=");
		builder.append(libelle);
		builder.append(", description=");
		builder.append(description);
		builder.append(", groupeId=");
		builder.append(groupeId);
		builder.append(", actif=");
		builder.append(actif);
		builder.append("]");
		return builder.toString();
	}




}
