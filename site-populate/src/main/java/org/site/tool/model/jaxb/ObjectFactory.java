
package org.site.tool.model.jaxb;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.site.tool.model.jaxb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Menus_QNAME = new QName("", "menus");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.site.tool.model.jaxb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MenusType }
     * 
     */
    public MenusType createMenusType() {
        return new MenusType();
    }

    /**
     * Create an instance of {@link ItemType }
     * 
     */
    public ItemType createItemType() {
        return new ItemType();
    }

    /**
     * Create an instance of {@link MenuType }
     * 
     */
    public MenuType createMenuType() {
        return new MenuType();
    }

    /**
     * Create an instance of {@link SousMenuItemType }
     * 
     */
    public SousMenuItemType createSousMenuItemType() {
        return new SousMenuItemType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MenusType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "menus")
    public JAXBElement<MenusType> createMenus(MenusType value) {
        return new JAXBElement<MenusType>(_Menus_QNAME, MenusType.class, null, value);
    }

}
