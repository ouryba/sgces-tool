package org.site.tool.model;

public class Menu {

	private long id;
	private String code;
	private String libelle;
	private boolean actif;

	private String icon;
	private String borderStyle;
	private boolean borderTop;
	private boolean showmenu;
	private String action;


	public Menu() {
	}

	public Menu(long id, String code, String libelle, boolean actif) {
		super();
		this.id = id;
		this.code = code;
		this.libelle = libelle;
		this.actif = actif;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public boolean isActif() {
		return actif;
	}
	public void setActif(boolean actif) {
		this.actif = actif;
	}


	// Additionnal attributes


	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getBorderStyle() {
		return borderStyle;
	}

	public void setBorderStyle(String borderStyle) {
		this.borderStyle = borderStyle;
	}

	public boolean isBorderTop() {
		return borderTop;
	}

	public void setBorderTop(boolean borderTop) {
		this.borderTop = borderTop;
	}

	public boolean isShowmenu() {
		return showmenu;
	}

	public void setShowmenu(boolean showmenu) {
		this.showmenu = showmenu;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Menu [id=");
		builder.append(id);
		builder.append(", code=");
		builder.append(code);
		builder.append(", libelle=");
		builder.append(libelle);
		builder.append(", actif=");
		builder.append(actif);
		builder.append("]");
		return builder.toString();
	}


}
