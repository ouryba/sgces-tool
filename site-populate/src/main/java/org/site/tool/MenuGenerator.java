package org.site.tool;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.site.tool.model.ArticleGroupe;
import org.site.tool.model.ArticleType;
import org.site.tool.model.Menu;
import org.site.tool.model.jaxb.ItemType;
import org.site.tool.model.jaxb.MenuType;
import org.site.tool.model.jaxb.MenusType;
import org.site.tool.model.jaxb.ObjectFactory;
import org.site.tool.model.jaxb.SousMenuItemType;

public class MenuGenerator {

	
	Logger logger = Logger.getLogger("MenuGenerator");
	ConnectionManager conManager;


	public MenuGenerator() {
		conManager = new ConnectionManager();
	}






	public List<Menu> getListMenus() {
		String query = "SELECT id, code, libelle, actif, borderStyle, borderTop, showmenu, action FROM b01_menu order by ordre";
		List<Menu> menus = new ArrayList<>();
		PreparedStatement pst = null;
		ResultSet rset = null;
		try {
			pst = conManager.getPreparedStatement(query);
			rset = pst.executeQuery();
			while(rset.next()) {
				Menu m = new Menu(rset.getLong(1), rset.getString(2), rset.getString(3), rset.getBoolean(4));
				m.setBorderStyle(rset.getString(5));
				m.setBorderTop(rset.getBoolean(6));
				m.setShowmenu(rset.getBoolean(7));
				m.setAction(rset.getString(8));;
				menus.add( m );

				logger.log(Level.INFO, m.toString());
			}
		} catch (Exception e) {
			logger.log(Level.INFO, "[ERROR]", e);
		} finally { 
			conManager.closePreparedStatement(pst, rset);
		}

		return menus;
	}


	public List<ArticleGroupe> getListGroupeByMenu(Menu menu) {
		String query = "SELECT id, code, name, description, image, menu_id acitf FROM b01_articlegrp where menu_id=?";
		List<ArticleGroupe> articleGroupes = new ArrayList<ArticleGroupe>();

		PreparedStatement pst = null;
		ResultSet rset = null;
		try {
			pst = conManager.getPreparedStatement(query);
			pst.setLong(1, menu.getId());

			rset = pst.executeQuery();
			while(rset.next()) {
				ArticleGroupe grp =  new ArticleGroupe(rset.getLong(1), rset.getString(2), rset.getString(3), rset.getString(4), rset.getBytes(5), rset.getLong(6));
				articleGroupes.add( grp );

				logger.log(Level.INFO, "\t "+grp);
			}
		} catch (Exception e) {
			logger.log(Level.INFO, "[ERROR]", e);
		} finally { 
			conManager.closePreparedStatement(pst, rset);
		}

		return articleGroupes;
	}



	public List<ArticleType> getListArticeTypesByGroupe(ArticleGroupe grp) {
		List<ArticleType> articleTypes = new ArrayList<ArticleType>();
		String query = "SELECT id, code, libelle, description, groupe_id, actif FROM b01_articletype WHERE groupe_id=?";
		PreparedStatement pst = null;
		ResultSet rset = null;
		try {
			pst = conManager.getPreparedStatement(query);
			pst.setLong(1, grp.getId());
			rset = pst.executeQuery();
			while(rset.next()) {
				ArticleType type = new ArticleType(rset.getLong(1), rset.getString(2), rset.getString(3), rset.getString(4), rset.getLong(5), rset.getBoolean(6));
				articleTypes.add(type);

				logger.log(Level.INFO, "\t\t "+type);
			}
		} catch (Exception e) {
			logger.log(Level.INFO, "[ERROR]", e);
		} finally { 
			conManager.closePreparedStatement(pst, rset);
		}

		return articleTypes;
	}




	/**
	 * Generate this element : 
	 * <pre>
	 * 		<menus haut="true" droite="false" bas="false" gauche="true" menuBorderTop="true" 
	 * 			menuOvale="true" hautOvale="true" droiteOvale="true" basOvale="true" gaucheOvale="true" 
	 * 			centreOvale="true" centreTabbed="true" contentPopup="true"
	 * 			showAlertes="false" alertesOvale="true" title="Barre des menus">
	 * </pre>
	 * 
	 * @return
	 */
	public MenusType createMenusType() {
		MenusType root = new MenusType();

		 root.setTitle("Barre des menus");

		 root.setHaut("true");
		 root.setBas("true");
		 root.setGauche("true");
		 root.setDroite("false");

		 root.setMenuOvale("true");
		 root.setMenuBorderTop("true");

		 root.setHautOvale("true");
		 root.setBasOvale("true");
		 root.setGaucheOvale("true");
		 root.setDroiteOvale("true");
		 
		 root.setCentreOvale("false");
		 root.setCentreTabbed("true");

		 root.setContentPopup("true");

		 root.setShowAlertes("false");
		 root.setAlertesOvale("true");

		 return root;
	}


	/**
	 * Generate this element :
	 * <pre>
	 * 		<menu id="chambre_et_salon" title="Chambre &amp; Salon" 
	 * 			icon="background:url('images/menus.gif') no-repeat 0px -16px;" 
	 * 			borderStyle="black" borderTop="false" showmenu="true" action="draw_menu_content(0);">
	 * </pre>
	 * @param menu
	 * @return
	 */
	public MenuType createMenuType(Menu menu) {
		MenuType menuType = new MenuType();
		
		menuType.setTitle(menu.getLibelle());
		menuType.setId(String.valueOf(menu.getId()));
		menuType.setShowmenu(String.valueOf(menu.isActif()));

		menuType.setIcon(menu.getIcon());
		menuType.setBorderStyle(menu.getBorderStyle());
		menuType.setBorderTop(String.valueOf(menu.isBorderTop()));
		menuType.setAction(menu.getAction());
		return menuType;
	}

	/**
	 * Genrate this element :
	 * 	<pre>
	 *  	<item name="Tous les canapés" code="CANAPES_FAUTEUIL" action="">
	 *  </pre>
	 *  
	 * @param groupe
	 * @return
	 */
	public ItemType createItemType(ArticleGroupe groupe) {
		ItemType itemType = new ItemType();
		itemType.setName(groupe.getDescription());
		itemType.setCode(groupe.getCode());
		itemType.setAction("");
		return itemType;
	}



	/**
	 * Generate this element :
	 * <pre>
	 * 		<sous-menu-item name="Canapé" action=""/>
	 * </pre>
	 * @param articleType
	 * @param articleGroupe 
	 * @return
	 */
	public SousMenuItemType createSousItemType(ArticleType articleType, ArticleGroupe articleGroupe) {
		if (!articleType.isActif()) {
			return null;
		}

		SousMenuItemType sousMenuItemType = new SousMenuItemType();
		sousMenuItemType.setName(articleType.getLibelle());

		StringBuilder sb = new StringBuilder();
		sb.append("sousMenuItemRechercherArticle(")
		.append("'").append(articleType.getLibelle().replaceAll("'", "\\\\\\\\'")).append("', ")
		.append(articleType.getId()).append(", ")
		.append(articleGroupe.getId())
		.append(")")
		;
		sousMenuItemType.setAction(sb.toString());

		return sousMenuItemType;
	}











	public static void main(String[] args) {
		MenuGenerator mg = new MenuGenerator();
		/*for (Menu m : mg.getListMenus()) {
			for (ArticleGroupe groupe : mg.getListGroupeByMenu(m)) {
				mg.getListArticeTypesByGroupe(groupe);
			}
		}*/

		mg.generateXmlFile();

	}




	public MenusType buildRootElement() {
		MenusType rootElement = this.createMenusType();

		for (Menu menu : getListMenus()) {
			MenuType menuType = this.createMenuType(menu);
			rootElement.getMenu().add(menuType);

			for (ArticleGroupe articleGroupe : this.getListGroupeByMenu(menu)) {
				ItemType itemType = this.createItemType(articleGroupe);
				menuType.getItem().add(itemType);

				for (ArticleType articleType : this.getListArticeTypesByGroupe(articleGroupe)) {
					if (articleType.isActif()) {
						SousMenuItemType sousMenuItemType = this.createSousItemType(articleType, articleGroupe);
						itemType.getSousMenuItem().add(sousMenuItemType);
					}
				}
			}
		}
		return rootElement;
	}

	public void generateXmlFile() {
		MenusType root = buildRootElement();

		try (OutputStream out = new FileOutputStream("../../sgces/content/fr/menu/menu.xml")) {
			this.marshal(root, out, ObjectFactory.class);
		} catch (JAXBException | IOException e) {
			logger.log(Level.INFO, "[ERROR]", e);
		}

	}







	/**
	 * Méthode pour créer un objet  à partir d'un flux XML.
	 * 
	 * @param inputStream
	 *		le flux XML.
	 * @param classesToBeBound
	 *		Classes à reconnaitre
	 * @return U
	 * @throws JAXBException 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <U> U unmarshal(final InputStream inputStream, Class... classesToBeBound ) throws JAXBException 
	{
		U u = null;

		try {
			final JAXBContext jaxbContext = JAXBContext.newInstance(classesToBeBound);

			final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			u = ((JAXBElement<U>) unmarshaller.unmarshal(inputStream)).getValue();

		} catch (JAXBException e) {
			logger.log(Level.INFO, "[ERROR]", e);
			throw new JAXBException("Erreur lors de la création à partir d'un flux xml.", e);
		}

		return u;
	}

	/**
	 * Méthode pour créer le flux XML d'un objet T.
	 * 
	 * @param t
	 *		l'objet générique.
	 * @param classesToBeBound
	 *		Classes à reconnaitre
	 * @param outputStream
	 *		la sortie pour envoyer le flux XML.
	 * @throws JAXBException 
	 */
	@SuppressWarnings("rawtypes")
	public <U> void marshal(final U u, final OutputStream outputStream, Class... classesToBeBound) throws JAXBException
	{
		try {
			final JAXBContext context = JAXBContext.newInstance(classesToBeBound);
			final Marshaller marshaller = context.createMarshaller();

			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.marshal(u, outputStream);

		}
		catch (JAXBException e) {
			logger.log(Level.INFO, "[ERROR]", e);
			throw new JAXBException("Erreur lors de la création d'un flux xml à partir d'un " + u.getClass().getName(), e);
		}

	}



}
