package org.site.tool;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.log.MLevel;

public class ConnectionManager {


	String url = "jdbc:mysql://localhost:3306/sgces?autocommit=false&useSSL=false&serverTimezone=UTC";
	String user = "sgces";
	String password = "dbmba8sgces";

	
	
	public static final int NB_PREPARED_STATMENT = 1000;


	private Connection connection = null;
	private ComboPooledDataSource comboDS = null;


	
	private Integer nbPreparedStatment = 0;


	Logger logger = Logger.getLogger("com.mchange");

	{
		logger.setLevel(Level.INFO);
		com.mchange.v2.log.MLog.getLogger().setLevel(MLevel.INFO);
		System.setProperty("com.mchange.v2.log.MLog", "com.mchange.v2.log.FallbackMLog");
		System.setProperty("com.mchange.v2.log.FallbackMLog.DEFAULT_CUTOFF_LEVEL", "WARNING");

	}


	// 
	public Connection getConnection() {
		Connection con = null;
		try {

			if (comboDS == null) {
				// Load MySQL, PostgreSQL, Oracle...  driver
				DriverManager.registerDriver(new  com.mysql.jdbc.Driver());
				comboDS = new ComboPooledDataSource("injecteSgces");
				comboDS.setJdbcUrl(url);
				comboDS.setUser(user);
				comboDS.setPassword(password);
			} else {
				// : Can't call commit when autocommit=true
				//comboDS.getConnection().commit();
			}
			// ComboPooledDataSource comboDS = new ComboPooledDataSource();

			return comboDS.getConnectionPoolDataSource().getPooledConnection().getConnection();
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		}
		return con;
	}

	// 
	public Connection newConnection(Connection con) throws SQLException {
		Connection newCon = con;
		if (con == null) {
			newCon = getConnection();
			nbPreparedStatment = 0;
		} else {
			if (nbPreparedStatment >= NB_PREPARED_STATMENT) {
				newCon = newConnection(null);
				nbPreparedStatment = 0;
			} else {
				nbPreparedStatment += 2;
			}
		}
		if (newCon.isClosed()) {
			return comboDS.getConnection();
		} else {
			return newCon;
		}
	}


	public PreparedStatement getPreparedStatement(String query) 
			throws SQLException {
		connection = newConnection(connection);
		return connection.prepareStatement(query);
	}

	public void closePreparedStatement(PreparedStatement pst, ResultSet rset) {
		try {
			if (rset != null) {
				rset.close();
			}
			if (pst != null) {
				pst.close();
			}
			rset = null;
			pst = null;
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		}
	}

	public Statement getCreateStatement() throws SQLException {
		connection = newConnection(connection);
		return connection.createStatement();
	}


}
