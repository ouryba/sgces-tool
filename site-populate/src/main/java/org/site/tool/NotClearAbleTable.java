package org.site.tool;
public enum NotClearAbleTable {

	p01_parameter, 
	xarits, s01_profile, s02_operation,
	b01_articletype, b01_articlegrp,
	livredor, rss;

	public static boolean isDeletable(String name) {
		for (NotClearAbleTable type : NotClearAbleTable.values()) {
			if (type.name().equalsIgnoreCase(name)) {
				return false;
			}
		}
		return true;
	}

}