package org.site.tool;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.site.tool.model.ArticleGroupe;
import org.site.tool.model.ArticleType;

import com.mchange.v2.log.MLevel;

public class JdbcSgcesTool {

	//
//	String url = "jdbc:mysql://127.0.0.1:3306/sgces?autocommit=false&useSSL=false&serverTimezone=UTC";
//	String user = "sgces";
//	String password = "dbmba8sgces";

	//
	boolean debug = true; 
	boolean debugDialog = false; 
	String iconDir = "C:\\Users\\Administrateur\\Downloads\\fruitsIcons\\";


	//public static final int NB_ARTICLE_TYPES = 30;
	//public static final int NB_ARTICLE_GROUPES = 6;

	//public static final int NB_ARTICLES = 100;
	public static final int NB_ARTICLE_PAR_TYPE = 1;
	public static final int NB_DESCRIPTION_PAR_ARTICLE = 2;
	public static final int NB_IMAGE_PAR_ARTICLE = 1;
	public static final int NB_AVIS_PAR_ARTICLE = 5;
	public static final int NB_INCONNUS = 100;
	public static final int NB_PREPARED_STATMENT = 500;

	private Connection connection = null;
	ConnectionManager conManager;


	Logger logger = Logger.getLogger("com.mchange");
	//
	{
		logger.setLevel(Level.ALL);
		com.mchange.v2.log.MLog.getLogger().setLevel(MLevel.INFO);
		System.setProperty("com.mchange.v2.log.MLog", "com.mchange.v2.log.FallbackMLog");
		System.setProperty("com.mchange.v2.log.FallbackMLog.DEFAULT_CUTOFF_LEVEL", "WARNING");

	}

	public JdbcSgcesTool() {
		conManager = new ConnectionManager();
	}


	// 
	public Connection getConnection() throws SQLException  {
		return conManager.newConnection(null);
	}

	// 
	public Connection initConnection(Connection con) throws SQLException {
		return conManager.newConnection(con);
	}

	/** ======================================================================== */
	/** ======================================================================== */

	public void injecteArticles(int nbArticles, Integer type_id) throws FileNotFoundException {
		ArticleType articleType = getArticleTypeById(type_id);
		ArticleGroupe articleGroupe = getArticleGroupeByType(articleType); 

		for (int i = 0; i < nbArticles; i++) {
			 String libelle = "Libelle article "+i;
			 String description = "<b>Description articles "+i + "</b><br>"+getDescription();
			 File file = getImageFile(articleGroupe, articleType); 
			 InputStream image = new  FileInputStream(file);
			 String image_name = file.getName();
			 boolean actif = true;

			 injecteArticle(type_id, libelle, description, image, image_name, actif);
		 }
	}



	public ArticleGroupe getArticleGroupeByType(ArticleType articleType) {
		String query = "SELECT id, code, name, description, image, menu_id acitf FROM b01_articlegrp where id=?";

		if (articleType == null) {
			return null;
		}

		PreparedStatement pst = null;
		ResultSet rset = null;
		try {
			pst = conManager.getPreparedStatement(query);
			pst.setLong(1, articleType.getGroupeId());

			rset = pst.executeQuery();
			while(rset.next()) {
				return new ArticleGroupe(rset.getLong(1), rset.getString(2), rset.getString(3), rset.getString(4), rset.getBytes(5), rset.getLong(6));
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		} finally { 
			conManager.closePreparedStatement(pst, rset);
		}

		return null;
	}
	public ArticleType getArticleTypeById(Integer typeId) {
		String query = "SELECT id, code, libelle, description, groupe_id, actif FROM b01_articletype WHERE id=?";

		PreparedStatement pst = null;
		ResultSet rset = null;
		try {
			pst = conManager.getPreparedStatement(query);
			pst.setLong(1, typeId);

			rset = pst.executeQuery();
			while(rset.next()) {
				// long id, String code, String libelle, String description, long groupeId, boolean actif
				return new ArticleType(rset.getLong(1), rset.getString(2), rset.getString(3), rset.getString(4), rset.getLong(5), rset.getBoolean(6));
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		} finally { 
			conManager.closePreparedStatement(pst, rset);
		}

		return null;
	}


	public void injecteArticlesFromType(ArticleType type) throws FileNotFoundException {
		ArticleGroupe articleGroupe = getArticleGroupeByType(type);

		try {
			if (type != null) {
				for (int i = 0; i < NB_ARTICLE_PAR_TYPE; i++) {
					String libelle = type.getLibelle();
					String description = "<b>"+type.getLibelle() + "</b><br>"+getDescription();
					File file = getImageFile(articleGroupe, type); 
					try (InputStream image = new  FileInputStream(file)) {
						String imageName = file.getName();
						boolean actif = true;
		
						injecteArticle((int)type.getId(), libelle, description, image, imageName, actif);
					}
				}
			} else {
				logger.log(Level.INFO, "null");
			}
		} catch(Exception e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		}
	}

	public void injecteArticle(
			Integer type_id, 	// integer not null,
			String libelle, 	// varchar(255),
			String description, // mediumblob,
			InputStream image, 	// mediumblob, 
			String image_name, 	// varchar(255),
			boolean actif		//boolean default true
	) {
		String query = "insert into b02_article ("
				+ " type_id, libelle, description, image, image_name, actif,  montant,recommande,discount "
				+ " ) VALUES (?, ?, ?, ?, ?, ?,  ?,?,?)";
		try (PreparedStatement pst = conManager.getPreparedStatement(query)) {

			pst.setInt(1, type_id);
			pst.setString(2, libelle);
			pst.setString(3, description);
			pst.setObject(4, image);
			pst.setString(5, image_name);
			pst.setBoolean(6, actif);

			pst.setDouble(7,   (Math.random() * 1000));
			pst.setBoolean(8, Math.random() > .9);
			double discount = Math.random(); 
			pst.setDouble(9, discount > 0.9 ? discount/2 : 0);

			int result = pst.executeUpdate();

			if (result == 1) {
				if (debug) {
					Integer article_id = getLastInsertId();
					logger.log(Level.INFO, "\t\t> CREATION ARTICLE ......................................... OK - ID : " + article_id);
					if (article_id != 0) {
						injecteImages(NB_IMAGE_PAR_ARTICLE, article_id, type_id);
						injecteDescriptions(NB_DESCRIPTION_PAR_ARTICLE, article_id);
						injecteAviss(NB_AVIS_PAR_ARTICLE, article_id);
					}
				}
			} else
				logger.log(Level.INFO, "\t\t> CREATION ARTICLE ......................................... KO");
		} catch (Exception e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		}
	}
	public void clearArticle() throws SQLException {
		clearTable("", null);
	}



	/** ======================================================================== */



	public void injecteImages(int nbImages, Integer article_id, Integer type_id) throws FileNotFoundException {
		ArticleType articleType = getArticleTypeById(type_id);
		ArticleGroupe articleGroupe = getArticleGroupeByType(articleType); 

		 for (int i = 1; i < nbImages; i++) {
			 File file = getImageFile(articleGroupe, articleType); 
			 Icon icone = new ImageIcon(file.getAbsolutePath());

			 String name = file.getName(); 					// varchar(255),
			 Integer width = icone.getIconWidth(); 		// integer(3),
			 Integer height = icone.getIconHeight();	// integer(3),
			 Integer size = (int)file.length(); 					// integer(6),
			 InputStream rawdata = new  FileInputStream(file); 			// mediumblob not null,
			 String type = name.substring(name.lastIndexOf(".")+1); 	// varchar(32) not null,
			 String description = "Description "+i+" article "+article_id;// varchar(255) default null,

			 injecteImage(name, width, height, size, rawdata, type, description, article_id);
		 }
	}
	public void injecteImage(
			String name,
			Integer width, 
			Integer height, 
			Integer size, 
			InputStream rawdata, 
			String type,
			String description,
			Integer article_id
	) {
		String query = "insert into b03_image ("
				+ " name, width, height, size, rawdata, type, description, article_id" 
				+ " ) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		
		try (PreparedStatement pst = conManager.getPreparedStatement(query)) {
			pst.setString(1, name);
			pst.setInt(2, width);
			pst.setInt(3, height);
			pst.setInt(4, size);
			pst.setObject(5, rawdata);
			pst.setString(6, type);
			pst.setString(7, description);
			pst.setInt(8, article_id);

			int result = pst.executeUpdate();
			if (result == 1) {
				if (debug) {
					//logger.log(Level.INFO, "\t\t\t> CREATION IMAGE ......................................... OK");
				}
			} else
				//logger.log(Level.INFO, "\t\t\t> CREATION IMAGE ......................................... KO");
				conManager.closePreparedStatement(pst, null);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		}
	}
	public void clearImage() throws SQLException {
		clearTable("", null);
	}

	/** ======================================================================== */
	public void injecteDescriptions(int nbDescriptions, Integer article_id) {
		 for (int i = 1; i < nbDescriptions; i++) {
				String libelle = "Description "+i+" de l'article "+article_id; 									// varchar(255),
				String text =  "<b>Description "+i+" de l'article "+article_id+ "</b><br>"+getDescription(); 	//  mediumblob,
				String type = "text"; 																			//  varchar(32) default null,

				injecteDescription(libelle, text, type, article_id);
		 }
	}
	public void injecteDescription(
			String libelle,
			String text,
			String type,
			Integer article_id
	) {
		String query = "insert into b04_description (libelle, text, type, article_id) VALUES (?, ?, ?, ?)";
		try (PreparedStatement pst = conManager.getPreparedStatement(query)) {

			pst.setString(1, libelle);
			pst.setString(2, text);
			pst.setString(3, type);
			pst.setInt(4, article_id);

			int result = pst.executeUpdate();

			if (result == 1) {
				if (debug) {
					logger.log(Level.INFO, "\t\t\t\t> CREATION DESCRIPTION ......................................... OK");
				}
			} else
				logger.log(Level.INFO, "\t\t\t\t> CREATION DESCRIPTION ......................................... KO");
			conManager.closePreparedStatement(pst, null);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		}
	}
	public void clearDescription() throws SQLException {
		clearTable("", null);
	}

	/** ======================================================================== */
	public void injecteAviss(int nbAvis, Integer article_id) {
		 for (int i = 1; i <= nbAvis; i++) {
			 String avis = "<b><u>Avis "+i+" de l'article "+article_id+ "</u></b><br>"+getAvis(); 	// blob not null,
			 Date date_avis = randomDate("2013-01-01", 365); 										// datetime not null,
			 Integer note_avis =  randomInt(6); 													// integer(1) default 0,
			 Integer user_id = (Integer) getIdFormListIds (
					 "select id from xarits"
			 ); 															// integer(1) default 0,

			 injecteAvis(avis, date_avis, note_avis, article_id, user_id);
		 }
	}
	public void injecteAvis(
			String avis,
			Date date_avis,
			Integer note_avis,
			Integer article_id,
			Integer user_id
	) {
		String query = "insert into c04_avis ("
				+ " avis, date_avis, note_avis, article_id, user_id "
				+ " ) VALUES (?, ?, ?, ?, ?)";
		try (PreparedStatement pst = conManager.getPreparedStatement(query)) {
			pst.setString(1, avis);
			pst.setDate(2, date_avis);
			pst.setInt(3, note_avis);
			pst.setInt(4, article_id);
			pst.setInt(5, user_id);

			int result = pst.executeUpdate();

			if (result == 1) {
				if (debug) {
					logger.log(Level.INFO, "\t\t\t\t\t> CREATION AVIS ......................................... OK");
				}
			} else
				logger.log(Level.INFO, "\t\t\t\t\t> CREATION AVIS ......................................... KO");
			conManager.closePreparedStatement(pst, null);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		}
	}
	public void clearAvis() throws SQLException {
		clearTable("", null);
	}

	/** ======================================================================== */
	public void injecteInconnus(int nbInconnus) {
		 for (int i = 1; i <= nbInconnus; i++) {
				String nom = "nom"+i; 				// varchar(32) default null,
				String prenom = "prenom"+i; 	// varchar(32) default null,
				Date date_creation = randomDate("2013-01-01", 365); //  datetime not null,
				String email = "inconnu"+i+"@gmail.com"; 	// varchar(255) not null,
				String cell_phone = "06.21.18.38.58"; 				// varchar(20) default null,
				boolean notify = yn(); 										// integer(1) default 0,
			 injecteInconnu(nom, prenom, date_creation, email, cell_phone, notify);
		 }
	}
	public void injecteInconnu(
			String nom,
			String prenom,
			Date date_creation,
			String email,
			String cell_phone,
			boolean notify
	) {
		String query = "insert into c01_inconnu ("
				+ " nom, prenom, date_creation, email, cell_phone, notify"
				+ ") VALUES (?, ?, ?, ?, ?, ?)";
		try (PreparedStatement pst = conManager.getPreparedStatement(query)) {
			pst.setString(1, nom);
			pst.setString(2, prenom);
			pst.setDate(3, date_creation);
			pst.setString(4, email);
			pst.setString(5, cell_phone);
			pst.setBoolean(6, notify);
			int result = pst.executeUpdate();

			if (result == 1) {
				if (debug) {
					//logger.log(Level.INFO, "\t\t> CREATION INCONNU ......................................... OK");
				}
			} else
				//logger.log(Level.INFO, "\t\t> CREATION INCONNU ......................................... KO");
				conManager.closePreparedStatement(pst, null);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		}
	}
	public void clearInconnu() throws SQLException {
		execute("delete from  c01_inconnu");
	}



	/** ======================================================================== */
	public void injectes(int nbLocations) {
		 for (int i = 1; i <= nbLocations; i++) {
			 injecte();
		 }
	}
	public void injecte(
	) {
		String query = "insert into  ("
				+ " ) VALUES (?, )";
		try (PreparedStatement pst = conManager.getPreparedStatement(query)) {
			int result = pst.executeUpdate();
			if (result == 1) {
				if (debug) {
					//logger.log(Level.INFO, "\t\t> CREATION  ......................................... OK");
				}
			} else
				//logger.log(Level.INFO, "\t\t> CREATION  ......................................... KO");
				conManager.closePreparedStatement(pst, null);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		}
	}
	public void clear() throws SQLException {
		clearTable("", null);
	}

	public String getDescription() {
		switch ((int) (Math.random() * 10)) {
			case 0:case 1:case 2:case 3:
				return "Ceci et un canapé. Ceci et un canapé. Ceci et un canapé. Ceci et un canapé. Ceci et un canapé. "
				+ "Ceci et un canapé. Ceci et un canapé. Ceci et un canapé. Ceci et un canapé. Ceci et un canapé. "
				+ "Ceci et un canapé. Ceci et un canapé. Ceci et un canapé. Ceci et un canapé. Ceci et un canapé. ";
			case 4:case 5:case 6:case 7: 
				return "Ceci et un lit. Ceci et un lit. Ceci et un lit. Ceci et un lit. Ceci et un lit. Ceci et un lit. "
				+ "Ceci et un lit. Ceci et un lit. Ceci et un lit. Ceci et un lit. Ceci et un lit. Ceci et un lit. "
				+ "Ceci et un lit. Ceci et un lit. Ceci et un lit. Ceci et un lit. Ceci et un lit. Ceci et un lit. ";
			default:
				return "Ceci et un test. Ceci et un test. Ceci et un test. Ceci et un test. "
				+ "Ceci et un test. Ceci et un test. Ceci et un test. Ceci et un test. Ceci et un test. "
				+ "Ceci et un test. Ceci et un test. Ceci et un test. Ceci et un test. Ceci et un test. ";
		}
		
	}
	public String getAvis() {
		return "Ceci et un avis. Ceci et un avis. Ceci et un avis. Ceci et un avis. "
		+ "Ceci et un avis. Ceci et un avis. Ceci et un avis. Ceci et un avis. Ceci et un avis. "
		+ "Ceci et un avis. Ceci et un avis. Ceci et un avis. Ceci et un avis. Ceci et un avis. "
		+ "Ceci et un avis. Ceci et un avis. Ceci et un avis. Ceci et un avis. Ceci et un avis. "
		+ "Ceci et un avis. Ceci et un avis. Ceci et un avis. Ceci et un avis. Ceci et un avis. ";
	}
	public File getImageFile(ArticleGroupe articleGroupe, ArticleType articleType) {

		File dir = new File(iconDir);

		if (articleGroupe != null) {
			ArticleGroupeEnum agEnum = ArticleGroupeEnum.parse(articleGroupe.getCode());
			if (agEnum != null) {
				String sousDir = agEnum.getImageDir();

				String type = articleType.getCode().toLowerCase()
						.replaceAll("-", File.separator+File.separator);

				dir = new File(iconDir + File.separator + sousDir + File.separator + type);

				if (dir.isDirectory() && dir.canRead()) {

					File [] files = dir.listFiles();
					File rf = files[randomInt(files.length)];
					if (rf.isFile()) {
						return rf;
					} else {
						return getImageFile(articleGroupe, articleType);
					}

				}

			}


		}

		return null;
	}

	/**
	 * ========================================================================
	 * ========================== UTILITAIRES ==================================
	 * ========================================================================
	 */

	public void execute(String request) throws SQLException {
		try {
			Statement stmt = conManager.getCreateStatement();
			stmt.execute(request);
			stmt.close();
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
			throw e;
		}
	}
	public void clearTable(String tbName, String sequence) throws SQLException {
		try {
			connection = initConnection(connection);
			try (Statement stmt = connection.createStatement()) {
				stmt.executeQuery("DELETE FROM " + tbName);
				stmt.close();
			}
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
			throw e;
		}
	}

	public BigDecimal getId(String query) {
		BigDecimal val = null;
		try (PreparedStatement pst = conManager.getPreparedStatement(query)) {
			try (ResultSet rset = pst.executeQuery()) {
				while (rset.next()) {
					val = rset.getBigDecimal(1);
					break;
				}
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		}
		return val;
	}

	public int getLastInsertId() {
		int r = 0;
		try (Statement stmt = conManager.getCreateStatement(); 
				ResultSet rset = stmt.executeQuery("SELECT LAST_INSERT_ID() as id") ) {
			if (rset.next()) {
				r = rset.getInt(1);
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		}
		return r;
	}

	/** ======================================================================== */
	public List<Object> getListIds(String query) {
		List<Object> listIds = new ArrayList<Object>();
		try (PreparedStatement pst = conManager.getPreparedStatement(query);
				ResultSet rset = pst.executeQuery()) {
			while (rset.next()) {
				listIds.add(rset.getObject(1));
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		}
		return listIds;
	}

	/** ======================================================================== */
	public Object getIdFormListIds(String query) {
		List<Object> listIds = getListIds(query);
		int r = listIds.size();
		if (r == 0) {
			return null;
		} else {
			return listIds.get(randomInt(r));
		}
	}

	/** ======================================================================== */
	public PreparedStatement getPreparedStatement(String query) 
		throws SQLException {
		connection = initConnection(connection);
		return connection.prepareStatement(query);
	}

	/** ======================================================================== */

	// ***********************************************************************************
	// ************************ JAVA HELPERS *********************************************
	// ***********************************************************************************

	public Date randomDate(String debut, int intervalle) {
		int r = 24 * 60 * 60 * 1000 * randomInt(intervalle);
		return new Date(r + getTime(debut));
	}

	public Integer randomInt(int max) {
		return (int) (Math.random() * (max));
	}
	public Integer randomZInt(int max) {
		int r = (int) (Math.random() * (max));
		if (r > max / 2) {
			return r;
		} else {
			return -r;
		}
	}

	public char randomChar(char c, char... values) {
		int s = values.length + 1;
		int r = randomInt(s);
		if (r == 0)
			return c;
		else {
			return values[r - 1];
		}
	}

	public char ynChar() {
		return randomChar('Y', 'N');
	}
	public boolean yn() {
		return randomChar('Y', 'N') == 'Y';
	}

	/** ======================================================================== */
	public java.util.Date getSqlDate(String chaine) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return sdf.parse(chaine);
		} catch (ParseException e) {
			logger.log(Level.INFO, "Exeption de format de date -> e1 !!!");
			try {
				sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				return sdf.parse(chaine);
			} catch (ParseException e1) {
				logger.log(Level.INFO, "Exeption de format de date -> e2 : DATE DU JOUR est retourne !!!");
			}
		}
		return new java.util.Date();
	}
	public long getTime(String strDate) {
		return getSqlDate(strDate).getTime();
	}

	// ***************************************************************************
	// * ************************ POST PROCESS MEMBERS ***********************
	// ***************************************************************************

	public void postInject() {
		Connection con = null;

		try {
			con = initConnection(null);
			// ...
		} catch (Exception e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		} finally {
			try {
				if (con != null){
					con.close();
				}
			} catch (SQLException e) {
				logger.log(Level.SEVERE, "[ERREUR]", e);
			}
		}
	}

	/** ======================================================================== */


	/** ======================================================================== */
	// ************************************************************************* */
	// ************************* MAIN PROGRAMM ******************************* */
	// ************************************************************************* */
	/** ======================================================================== */

	public static void main(String[] args) {
		JdbcSgcesTool jdbcTest = new JdbcSgcesTool();

		jdbcTest.start();


		List<ArticleType> articleTypes = jdbcTest.getAllArticleTypes();
		try {
			for (ArticleType type : articleTypes) {
				jdbcTest.injecteArticlesFromType(type);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		jdbcTest.finish();
	}

	/** ======================================================================== */
	
	/*public void showInputDialog() {
		String[] values = new String[] { "Oui - Continuer", "Non - Arreter" };

		String value = (String) JOptionPane.showInputDialog(null,
				"Voulez vous continuer a debugger/afficher les traces \n\t"
						+ url + " !", user, JOptionPane.DEFAULT_OPTION,
				new ImageIcon("./img/bt_green.gif"), values, values[0]);

		if (value != null && value.equals(values[1])) {
			this.debug = false;
		}
	}*/

	public void start() {
	}

	public void finish() {
		new MenuGenerator().generateXmlFile();
	}


	/** ======================================================================== */
	/** =============================== FIN ===================================== */
	/** ======================================================================== */



	public ArticleType getRandomArticleType() {
		ArticleType type = null;
		String query = "SELECT id, code, libelle, description, groupe_id, actif FROM b01_articletype";
		int r = 0;

		logger.log(Level.INFO, "==================== " + r + " ====================");
		logger.log(Level.INFO, "==================== " + r + " ====================");
		PreparedStatement pst = null;
		ResultSet rset = null;
		try {
			pst = conManager.getPreparedStatement(query);
			rset = pst.executeQuery();
			
			r = randomInt(rset.getFetchSize());
			for (int i = 0; i < r; i++) {
				rset.next();
			}
			if (rset.next()) {
				type = new ArticleType(rset.getLong(1), rset.getString(2), rset.getString(3), rset.getString(4), rset.getLong(5), rset.getBoolean(6));
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		} finally { 
			conManager.closePreparedStatement(pst, rset);
		}
		if (type == null ) {
			logger.log(Level.INFO, "nulll");
		}
		return type;
	}

	public List<ArticleType> getAllArticleTypes() {
		List<ArticleType> articleTypes = new ArrayList<ArticleType>();
		String query = "SELECT id, code, libelle, description, groupe_id, actif FROM b01_articletype";
		PreparedStatement pst = null;
		ResultSet rset = null;
		try {
			pst = conManager.getPreparedStatement(query);
			rset = pst.executeQuery();
			while(rset.next()) {
				articleTypes.add( new ArticleType(rset.getLong(1), rset.getString(2), rset.getString(3), rset.getString(4), rset.getLong(5), rset.getBoolean(6)) );
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "[ERREUR]", e);
		} finally { 
			conManager.closePreparedStatement(pst, rset);
		}
		return articleTypes;
	}
	
	
	
}
