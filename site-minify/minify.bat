@echo off

rem ================================================================================================================================
rem Install
rem First make sure you have installed the latest version of node.js (You may need to restart your computer after this step).

rem From NPM for use as a command line app:
rem npm install uglify-js -g

rem From NPM for programmatic use:
rem npm install uglify-js

rem ================================================================================================================================

set ROOT_JS_DIR=%cd%\..\..\sgces\js
set ROOT_JS_LIB_DIR=%cd%\..\..\sgces\lib

set OUT_JS_DIR=%cd%\..\..\sgces\out\js\


cd %ROOT_JS_DIR%\

mkdir %OUT_JS_DIR%

cmd /c uglifyjs blocks.js       -o %OUT_JS_DIR%\blocks-min.js  -c -m

rem cmd /c uglifyjs Cookies.js      -o %OUT_JS_DIR%\Cookies-min.js -c -m
cmd /c uglifyjs debug.js        -o %OUT_JS_DIR%\debug-min.js   -c -m
cmd /c uglifyjs extras.js       -o %OUT_JS_DIR%\extras-min.js  -c -m
cmd /c uglifyjs icons.js        -o %OUT_JS_DIR%\icons-min.js   -c -m

cmd /c uglifyjs onglets.js      -o %OUT_JS_DIR%\onglets-min.js   -c -m
cmd /c uglifyjs openimage.js    -o %OUT_JS_DIR%\openimage-min.js -c -m
cmd /c uglifyjs popup.js        -o %OUT_JS_DIR%\popup-min.js     -c -m
cmd /c uglifyjs proc.js         -o %OUT_JS_DIR%\proc-min.js      -c -m
cmd /c uglifyjs proc2.js        -o %OUT_JS_DIR%\proc2-min.js     -c -m

cmd /c uglifyjs tinyMCE.js      -o %OUT_JS_DIR%\tinyMCE-min.js    -c -m
cmd /c uglifyjs utils.fcts.js   -o %OUT_JS_DIR%\utils.fcts-min.js -c -m
cmd /c uglifyjs utils.js        -o %OUT_JS_DIR%\utils-min.js      -c -m



cd %ROOT_JS_DIR%\api
mkdir %OUT_JS_DIR%\api

cmd /c uglifyjs googlemap.js   -o %OUT_JS_DIR%\api\googlemap-min.js     -c -m
cmd /c uglifyjs leafletmap.js  -o %OUT_JS_DIR%\api\leafletmap-min.js     -c -m


cd %ROOT_JS_DIR%\article
mkdir %OUT_JS_DIR%\article

cmd /c uglifyjs adresseManager.js  -o %OUT_JS_DIR%\article\adresseManager-min.js     -c -m
cmd /c uglifyjs paiementManager.js -o %OUT_JS_DIR%\article\paiementManager-min.js    -c -m
cmd /c uglifyjs workflowManager.js  -o %OUT_JS_DIR%\article\workflowManager-min.js   -c -m
cmd /c uglifyjs article.js        -o %OUT_JS_DIR%\article\article-min.js     -c -m
cmd /c uglifyjs articleManager.js -o %OUT_JS_DIR%\article\articleManager-min.js     -c -m
cmd /c uglifyjs articlegrp.js     -o %OUT_JS_DIR%\article\articlegrp-min.js  -c -m
cmd /c uglifyjs articletype.js    -o %OUT_JS_DIR%\article\articletype-min.js -c -m
cmd /c uglifyjs reservation.js    -o %OUT_JS_DIR%\article\reservation-min.js -c -m
cmd /c uglifyjs transaction.js    -o %OUT_JS_DIR%\article\transaction-min.js -c -m
cmd /c uglifyjs search.js         -o %OUT_JS_DIR%\article\search-min.js      -c -m

cd  %ROOT_JS_DIR%\cadre
mkdir %OUT_JS_DIR%\cadre\

cmd /c uglifyjs style1.js        -o %OUT_JS_DIR%\cadre\style1-min.js -c -m
cmd /c uglifyjs style2.js        -o %OUT_JS_DIR%\cadre\style2-min.js -c -m
cmd /c uglifyjs style4.js        -o %OUT_JS_DIR%\cadre\style4-min.js -c -m

cd %ROOT_JS_DIR%\menu
mkdir %OUT_JS_DIR%\menu\

cmd /c uglifyjs menuManager.js   -o %OUT_JS_DIR%\menu\menuManager-min.js -c -m
cmd /c uglifyjs common.js        -o %OUT_JS_DIR%\menu\common-min.js -c -m
cmd /c uglifyjs style1.js        -o %OUT_JS_DIR%\menu\style1-min.js -c -m
cmd /c uglifyjs style2.js        -o %OUT_JS_DIR%\menu\style2-min.js -c -m
cmd /c uglifyjs style3.js        -o %OUT_JS_DIR%\menu\style3-min.js -c -m
cmd /c uglifyjs style4.js        -o %OUT_JS_DIR%\menu\style4-min.js -c -m


cd %ROOT_JS_DIR%\promo\
mkdir %OUT_JS_DIR%\promo\
cmd /c uglifyjs sliderManager.js -o %OUT_JS_DIR%\promo\sliderManager-min.js -c -m


cd %ROOT_JS_DIR%\partenaire\
mkdir %OUT_JS_DIR%\partenaire\
cmd /c uglifyjs partenaire.js -o %OUT_JS_DIR%\partenaire\partenaire-min.js -c -m

// oauth
cd %ROOT_JS_DIR%\oauth\
mkdir %OUT_JS_DIR%\oauth\
cmd /c uglifyjs facebook.js -o %OUT_JS_DIR%\oauth\facebook-min.js -c -m
cmd /c uglifyjs google.js -o %OUT_JS_DIR%\oauth\google-min.js -c -m



rem SkySlider component
cd %ROOT_JS_LIB_DIR%\skySlider\js\
mkdir %OUT_JS_DIR%\skySlider\js\

cmd /c uglifyjs slider.js -o %OUT_JS_DIR%\skySlider\js\slider-min.js -c -m


cd %~dp0

