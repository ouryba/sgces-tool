/**
 * Recursively read a source directory and do an obsfucation of all files that match /(-min.js)$/ regex
 * 
 * MAIN Program
 * 
 */

'use strict'

var gulp = require('gulp'),
javascriptObfuscator = require('gulp-javascript-obfuscator');

const fs = require('fs');
const path = require('path');
const { exec } = require('child_process');
const compressor = require('yuicompressor');

//const fsp = require("fs/promises"); //requires node 10.X
const {promisify} = require("util");  //requires node 8.X
const readdir = promisify(fs.readdir);
const readFile = promisify(fs.readFile);
const appendFile = promisify(fs.appendFile);




let CSS_SOURCE_DIR = "D:/apps/projects/perso/sgces/css/";
let CSS_OUTPUT_DIR = "D:/apps/projects/perso/sgces/out/css/";
let JS_SOURCE_DIR = "D:/apps/projects/perso/sgces/js/";
let JS_OUTPUT_DIR = "D:/apps/projects/perso/sgces/out/js/";

let SLIDER_JS_SOURCE_DIR= 'D:/apps/projects/perso/sgces/lib/skySlider/js';
let SLIDER_CSS_SOURCE_DIR = 'D:/apps/projects/perso/sgces/lib/skySlider/css';

var appendOptions = { menuGroupStyle : 3, cadreStyle: 2};




function minifyJS() {
	exec('cd .. && cmd /c minify.bat', (error, stdout, stderr) => {
		if (error) {
			displayError(error);
			console.error(`exec error: ${error}`);
			return;
		}
		console.log(`stdout: ${stdout}`);
		console.log(`stderr: ${stderr}`);
	});
}

async function minifyCSS(file, sourceDir, outDir) {

	if (/(-min\.css)$/.test(file)) {
		return 
	}
	const sourcePath = sourceDir + '/'+file;
	console.log('============================> ', sourcePath);

	await compressor.compress(sourcePath, {
		//Compressor Options:
		charset: 'utf8',
		type: 'css',					// js|css. This option is required if no input file has been specified. Otherwise, this option is only required if the input file extension is neither 'js' nor 'css' 
		nomunge: false, 				// Minify only. Do not obfuscate local symbols.
		'disable-optimizations': false 	// Disable all the built-in micro optimizations.
		, 'line-break': 180
	}, async function(err, data, extra) {
		//err   If compressor encounters an error, it's stderr will be here
		//data  The compressed string, you write it out where you want it
		//extra The stderr (warnings are printed here in case you want to echo them
		if (err) {
			displayError(err);
			if (extra) {
				displayError(extra);
			}
			return;
		} else if (extra) {
			displayError(extra);
			return;
		}

		//const outputFile = sourcePath.substring(0, (sourcePath.length-4)) + '-min.css';
		const outputFile = outDir+'/'+file.substring(0, (file.length-4)) + '-min.css';
		await fs.open(
				outputFile, 'w', 
				async (erreur, fd) => {
					if (erreur) {
						display(erreur);
						return;
					}
				
					await fs.write(fd, data, async (erreur2, written, str) => {
						if (erreur2) {
							display(erreur2);
						} else {
							//console.log('data : '+data);
							console.log('ouput file size '+outputFile+' : ' + written);
						}
					});
				}
		);
		
		//console.log(data);
	});
}



async function minifyCssDirectory(sourcePath, outDir) {
	try {
		fs.readdir(sourcePath, async (err, files) => {
			if (err) {
				console.log("minifyCssDirectory: error ==> " + err);
				displayError(err);
			} else {

				files.forEach( async (file) => {
					let process = true;
					const filePath = sourcePath + '/' + file;
					if (/^\./.test(file)) {
						console.log(filePath + " is a hidden file");
						process = false;
					}

					if (process) {
						const stats = fs.statSync(filePath);
						if (stats.isDirectory()) {
	
							await fs.mkdir(outDir+'/'+file, { recursive: true }, async (erreur) => {
								if (erreur && erreur.code != 'EEXIST') {
									console.log(erreur);
									//throw err;
									displayError(erreur);
								}
								 await minifyCssDirectory(filePath, outDir+'/'+file);
							});
	
						} else if(stats.isFile()) {
							await minifyCSS(file, sourcePath, outDir);
						} else {
							console.log(filePath, " is not a file or directory");
						}
					}
				});
			}
		})
	} catch (e) {
		displayError(e);
	} finally {
		console.log("finally");
	}
}




function obsfucateFile(file, outDir) {
	
	console.log('Obsfucating file ===> ', file);
	if (/(-min\.js)$/.test(file)) {
		console.log(file);
		gulp.src(file)
		.pipe(javascriptObfuscator({
			compact:true,
			sourceMap: true
		}))
		.pipe(gulp.dest(outDir));
	}

}

// Synthaxe
function obsfucateDir(sourcePath) {
	try {

		fs.readdir(sourcePath, (err, files) => {

			if (err) {
				console.log("Error ==> ");
				displayError(err);
				// process.exit(1);
			}
			
			files.forEach(file => {
				const filePath = sourcePath + '/' + file;

				if (/^\./.test(file)) {
					console.log(filePath + " is a hidden file");
					return;
				}

				/*const stats = fs.statSync(filePath);
				if (stats.isDirectory()) {
					obsfucateDir(filePath);
				} else if(stats.isFile()) {
					obsfucateFile(filePath, sourcePath);
				} else {
					console.log(filePath + " is not a file or directory");
				}*/

				fs.stat(filePath, function (err, stats) {
					if (err) {
						console.log('Error ==> ' + err);
						displayError(err);
						process.exit(1);
					} else {
						if (stats.isDirectory()) {
							obsfucateDir(filePath);
						} else if(stats.isFile()) {
							obsfucateFile(filePath, sourcePath);
						} else {
							console.log(filePath + " is not a file or directory");
						}
					}
				});

			});
		})
	} catch (e) {
		displayError(e);
	} finally {
		console.log("finally");
	}

}


//Synthaxe
function appendMinifiedFiles(sourcePath) {
	try {
		fs.readdir(sourcePath, (err, files) => {
			if (err) {
				console.log("Error ==> ");
				displayError(err);
			}
			files.forEach(file => {
				const filePath = sourcePath + '/' + file;
				if (/^\./.test(file)) {
					console.log(filePath + " is a hidden file");
					return;
				}
				const stats = fs.statSync(filePath);
				if (stats.isDirectory()) {
					appendMinifiedFiles(filePath);
				}
			});
		});

		appendObsfucated(sourcePath, sourcePath+'/'+'jssource-min.js');

	} catch (e) {
		console.log(e.stack);
		displayError(e);
	} finally {
		console.log("finally");
	}

}

// Append all minified and obsfucated files in source directory
// The resulting file is generated in the same directory 
function appendObsfucated(directory, destination) {
	readdir(directory)
		.then((files) => {
			// console.log('FILES CONTENT:', files);
			files.filter(file => {
				// console.log('FILTER > ' + file);
				if (file.indexOf('-min.js') != -1 && file.indexOf('-min.js.map') == -1) {
					if (directory.indexOf('menu') != -1 && file.indexOf('style') != -1) {
						return false; // (file.indexOf('style'+appendOptions.menuGroupStyle) != -1);
					} else if (directory.indexOf('cadre') != -1 && file.indexOf('style') != -1) {
						return (file.indexOf('style'+appendOptions.cadreStyle) != -1);
					}
					return true;
				} else {
					return false;
				}
			})
			.map(file => {
				console.log('MAP ('+destination+') > ' + path.join(directory, file));
				readFile(path.join(directory, file), 'utf8')
				.then(data => {
					//console.log('DATA:', data);
					appendFile(destination, data+'\n')
					.then(() => {
						console.log('append done <3');
					})
					.catch((err) => {
						displayError(err);
					});
				});
			});
		})
		.catch((err) => {
			console.log('ERROR: <', err);
			displayError(err);
		});
}


function displayError(e) {
	if (e instanceof TypeError) {
		// les instructions pour gérer TypeError
		console.error("TypeError: > ", e.message);
	} else if (e instanceof RangeError) {
		// les instructions pour gérer RangeError
		console.error("RangeError: > ", e.message);
	} else if (e instanceof EvalError) {
		// les instructions pour gérer EvalError
		console.error("EvalError: > ", e.message);
	} else {
		// les instructions pour gérer les autres exceptions
		console.error("ERROR Externe: <Unknown> - ",  e);
	}
}





//==================================================================================================

async function cleanDirectory(sourcePath, type) {
	try {
		fs.readdir(sourcePath, (err, files) => {
			if (err) {
				console.log("Cleaning directory error ==> " + err);
				displayError(err);
			} else {

				files.forEach(async (file) => {
					const filePath = sourcePath + '/' + file;
					if (/^\./.test(file)) {
						return;
					}

					const stats = fs.statSync(filePath);
					if (stats.isDirectory()) {
						cleanDirectory(filePath, type);
					} else if(stats.isFile()) {
						if (type === 'css' || type === 'js') {

							if (/\.*-min\.css/.test(file) || /(-min\.js)$/.test(file) || /(-min\.js\.map)$/.test(file)) {
								console.log('unlink file : ', filePath);
								fs.unlink(filePath, (erreur) => { 
									if (erreur) {
										displayError(erreur);
									}
								});
							}
						}
					} else {
						console.log(filePath + " is not a file or directory");
					}
				});
			}
		})
	} catch (e) {
		displayError(e);
	} finally {
		console.log("finally");
	}
}


// ==================================================================================================

(async function() {

const args = process.argv.slice(2);
console.log(args);  

if (args.length > 0) {
	if (args[0].toLowerCase() === '--clean' || args[0].toLowerCase() === 'clean' || args[0] === '-c' || args[0] === 'c') {
		cleanDirectory(CSS_OUTPUT_DIR, 'css');
		cleanDirectory(JS_OUTPUT_DIR, 'js');
	} 
	else if (args[0].toLowerCase() === '--minify' || args[0].toLowerCase() === 'minify' || args[0] === '-m' || args[0] === 'm') {
		await minifyCssDirectory(CSS_SOURCE_DIR, CSS_OUTPUT_DIR);
		await minifyCssDirectory(SLIDER_CSS_SOURCE_DIR, SLIDER_CSS_SOURCE_DIR);
		console.log("=== End CSS minification ===");

		minifyJS();
		obsfucateDir(JS_OUTPUT_DIR);
		obsfucateDir(SLIDER_JS_SOURCE_DIR);
	}
	else if (args[0].toLowerCase() === '--append' || args[0].toLowerCase() === 'append' || args[0] === '-a' || args[0] === 'a') {
		appendMinifiedFiles(JS_OUTPUT_DIR,);
	}
}
else {
	/*
	// =============================================
	process.argv.forEach((val, index) => {
		console.log(`${index}: ${val}`);
	});
	// =============================================
	*/

	console.log(`=====================+==============================================`);
	console.log(`==============  USAGE ==============================================`);
	console.log(`====================================================================`);
	console.log(`Clean minified files : node main.js --clean [clean|-c|c]`);
	console.log(`Generate minified files : node main.js --minify [minify|-m|m]`);
	console.log(`Append minified files : node main.js --append [append|-a|a]`);
	console.log(`Run all commandes c m a : node main.js --run [run|-r|r]`);
	console.log(`====================================================================`);

}


})();

// https://stackoverflow.com/questions/43728530/nodejs-concatenate-all-files-in-a-directory
/*
	If you're going to use bluebird than you get the benefit of promisification. You can use promisifyAll() to convert all error first 
	callback accepting async functions in the fs module to return a promise. You can read more about in the above promisification link.

	The below code reads in all of the files as strings and then reduces all of their contents into a single string and writes that string to the destination.

	Its probably best to not catch() any returned errors here. Rather, the caller should attach a catch() to handle any returned errors as they need.
*/

/*
const Promise = require('bluebird')
const path = require('path')

module.exports = (directory, destination) => {
	return fs.readdirAsync(directory)
	.filter(file => file['name'].indexOf('-min.js') != -1)
	.map(file => fs.readFileAsync(path.join(directory, file), 'utf8'))
	.then(contents => fs.writeFileAsync(destination, contents.join('\n')))
}*/

// ================= END ===============
// =====================================