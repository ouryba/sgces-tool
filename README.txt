mvn sonar:sonar \
  -Dsonar.projectKey=site-parent.org.site.tool \
  -Dsonar.organization=ouryba-bitbucket \
  -Dsonar.host.url=https://sonarcloud.io \
  -Dsonar.login=f32a9d3a7e25f3236e93ce09d32617be91dc5b7b

-- ============================================================================ --

REM D:\apps\projects\perso\sgces-tool

d:
cd D:\apps\projects\perso\sgces-tool

inst2xsd "../sgces/content/fr/menu/menu.xml" -outDir gen\. -enumerations never -outPrefix menu
xjc -verbose -extension -nv -npa -no-header -encoding utf8 -p org.site.tool.model.jaxb  -d .\src\main\java   gen\menu0.xsd


-- ============================================================================ --

JS Minification / Obsfucation 

@see https://www.npmjs.com/package/javascript-obfuscator
	 https://github.com/javascript-obfuscator/gulp-javascript-obfuscator/blob/master/index.js
	 https://www.npmjs.com/package/javascript-obfuscator
	 https://github.com/javascript-obfuscator/gulp-javascript-obfuscator


-- ============================================================================ --

CSS Minification / Obsfucation 

https://www.npmjs.com/package/yuicompressor

	npm i yuicompressor

